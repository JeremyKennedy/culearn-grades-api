# Filtering

## Get course by courseId
Filter syntax:
{"where":{"courseId":45305}}

REST syntax:
[where][courseId]=45305

URL syntax:
http://localhost:3000/api/courses?filter={"where":{"courseId":45305}}

## Get all courses by user
Filter syntax:
{"where":{"username":"jeremykennedy"}}

REST syntax:
[where][username]="jeremykennedy"

URL syntax:
http://localhost:3000/api/courses?filter={"where":{"username":"jeremykennedy"}}
