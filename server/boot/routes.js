// const x = require('student');

module.exports = app => {
  app.get('/ping', (req, res) => {
    let student = app.models.Student;
    let username = 'jeremykennedy';
    student.findOne({ username }, (err, { password }) => {
      res.send(password);
    });
  });
  app.get('/test', (req, res) => {
    res.render('index', { title: 'Hey', message: 'Hello there!' });
  });
  app.get('/courses', (req, res) => {
    let course = app.models.Course;
    let username = 'jeremykennedy';
    // course.find({ username }, (err,  coursesForUser ) => {
    course.find({ order: 'lastChanged DESC ' }, (err, coursesForUser) => {
      res.render('courses', { courses: coursesForUser });
    });
  });

  app.post('/toggle-state', ({ body }, res) => {
    app.models.Course.findOne({ courseId: body.courseId }, (err, foundCourse) => {
      foundCourse.updateAttribute('active', !foundCourse.active, (err, instance) => res.redirect('/courses'));
    });
  });
};
