const users = require('../privatevars.js');
module.exports = ({ dataSources, models }) => {
  dataSources.mongo.automigrate('student', err => {
    if (err) throw err;
    models.Student.create(
      [
        { username: users[0].username, password: users[0].password },
        { username: users[1].username, password: users[1].password }
      ],
      (err, student) => {
        if (err) throw err;
        // console.log('Models created: \n', student);
      }
    );
  });
};
