const cheerio = require('cheerio');
const request = require('request').defaults({ jar: true });
const loginURL = 'https://culearn.carleton.ca/moodle/login/index.php';
const headers = { 'Content-Type': 'application/x-www-form-urlencoded' };
const coursesURL = 'https://culearn.carleton.ca/moodle/my';
const gradesURL = 'https://culearn.carleton.ca/moodle/grade/report/user/index.php?id=';
const app = require('../../server/server.js');
const winston = require('winston');
const logger = new winston.Logger({
  transports: [new winston.transports.Console({ timestamp: true, colorize: true })]
});

module.exports = Course => {
  Course.refreshCourses = username => {
    return refreshCourses(username).catch(error => {
      logger.info('caught error for refreshCourses', error);
      return app.models.Student.doLogin(username).then(() => refreshCourses(username)).catch(error => {
        return error;
      });
    });
  };
  Course.remoteMethod('refreshCourses', {
    accepts: { arg: 'username', type: 'string' },
    returns: { arg: 'refreshed courses', type: 'array', root: true },
    http: { verb: 'POST' }
  });

  Course.keywordActivate = (username, keyword) => {
    return keywordActivate(username, keyword).catch(error => {
      logger.info('caught error for keywordActivate', error);
      return refreshCourses(username)
        .catch(error => {
          logger.info('caught error for refreshCourses', error);
          return app.models.Student.doLogin(username).then(() => refreshCourses(username)).catch(error => {
            return error;
          });
        })
        .then(() => keywordActivate(username, '2017'))
        .catch(error => {
          return error;
        });
    });
  };
  Course.remoteMethod('keywordActivate', {
    accepts: [{ arg: 'username', type: 'string' }, { arg: 'keyword', type: 'string' }],
    returns: { arg: 'count', type: 'integer', root: true },
    http: { verb: 'POST' }
  });

  Course.refreshGrade = (username, courseId) => {
    return refreshGrade(username, courseId);
  };
  Course.remoteMethod('refreshGrade', {
    accepts: [{ arg: 'username', type: 'string' }, { arg: 'courseId', type: 'integer' }],
    returns: { arg: 'refreshed course', type: 'object', root: true },
    http: { verb: 'POST' }
  });

  Course.refreshGradesActive = username => {
    logger.profile('FULL REFRESH');
    logger.info('FULL REFRESH STARTING');
    // TODO: make this less ugly
    return refreshGradesActive(username)
      .catch(error => {
        logger.info('caught error for refreshGradesActive', error);
        if (error === 'incorrect user') {
          return app.models.Student.doLogin(username).then(() => refreshGradesActive(username)).catch(error => {
            return error;
          });
        } else {
          return keywordActivate(username, '2017')
            .catch(error => {
              logger.info('caught error for keywordActivate', error);
              return refreshCourses(username)
                .catch(error => {
                  logger.info('caught error for refreshCourses', error);
                  return app.models.Student.doLogin(username).then(() => refreshCourses(username)).catch(error => {
                    return error;
                  });
                })
                .then(() => keywordActivate(username, '2017'))
                .catch(error => {
                  return error;
                });
            })
            .then(() => refreshGradesActive(username))
            .catch(error => {
              return error;
            });
        }
      })
      .then(courses => {
        logger.profile('FULL REFRESH');
        logger.info('FULL REFRESH DONE');
        return courses;
      });
  };
  Course.remoteMethod('refreshGradesActive', {
    accepts: { arg: 'username', type: 'string' },
    returns: { arg: 'refreshed grades', type: 'array', root: true },
    http: { verb: 'POST' }
  });

  Course.clear = () => {
    return new Promise((resolve, reject) => {
      Course.destroyAll({}, (err, info) => resolve(info));
    });
  };
  Course.remoteMethod('clear', { returns: { arg: 'cleared', type: 'string' }, http: { verb: 'POST' } });
};

function refreshCourses(username) {
  logger.profile(`refreshCourses ${username}`);
  logger.info('starting refreshCourses', { username });
  let Course = app.models.Course;
  return new Promise((resolve, reject) => {
    request.get(coursesURL, (error, response, body) => {
      let scrapedCourses = cheerio.load(body)('.course_link:first-child');
      if (scrapedCourses.length === 0) {
        logger.profile(`refreshCourses ${username}`);
        logger.info('rejected refreshCourses', { username });
        return reject('user not signed in');
        // TODO: move init function here from android app
      }
      let parsed = [];
      for (let i = 0; i < scrapedCourses.length; i++) {
        const courseId = scrapedCourses[i].attribs.href.replace(/^\D+/g, '');
        const name = scrapedCourses[i].children[0].data;
        parsed.push({ username, name, courseId });
      }
      Course.create(parsed, (err, model) => {
        Course.find({ where: { username: new RegExp(username, 'i') } }, (err, courses) => {
          logger.profile(`refreshCourses ${username}`);
          logger.info('finishing refreshCourses', { username });
          resolve(courses);
        });
      });
    });
  });
}

function keywordActivate(username, keyword) {
  logger.profile(`keywordActivate ${username}`);
  logger.info('starting keywordActivate', { username, keyword });
  let Course = app.models.Course;
  return new Promise((resolve, reject) => {
    if (keyword === undefined) {
      keyword = '2017';
      logger.info('keyword is now', keyword);
    }
    Course.updateAll(
      { and: [{ username: new RegExp(username, 'i'), name: new RegExp(keyword, 'i') }] },
      { active: true },
      (err, { count }) => {
        logger.profile(`keywordActivate ${username}`);
        if (count > 0) {
          logger.info('finishing keywordActivate', { username, keyword, count });
          resolve(count);
        } else {
          logger.info('rejected keywordActivate', { username, keyword, count });
          reject('no courses matching keyword');
        }
      }
    );
  });
}

function refreshGrade(username, courseId) {
  logger.profile(`refreshGrade ${courseId}`);
  logger.info('starting refreshGrade', { username, courseId });
  let Course = app.models.Course;
  return new Promise((resolve, reject) => {
    request.post({ url: gradesURL + courseId, headers }, (error, response, body) => {
      let currentUser = cheerio.load(body)("[title='View profile']").text();
      currentUser = currentUser.slice(currentUser.length / 2);
      if (currentUser.toLowerCase().substr(0, 3) !== username.toLowerCase().substr(0, 3)) {
        logger.profile(`refreshGrade ${courseId}`);
        logger.info('rejecting refreshGrade', { username, courseId, currentUser });
        return reject('incorrect user');
      }
      const grades = cheerio.load(body)('.user-grade').toString();
      let gradesParsed = [];
      //noinspection JSUndeclaredVariable
      $ = cheerio.load(body, { normalizeWhitespace: true });
      $('tbody tr').each(function(i, element) {
        let title = $(this).children('.column-itemname').text().trim();
        title = title.replace('Include empty grades.', '').replace('Weighted mean of grades.', '').trim();
        gradesParsed.push({ title });
        gradesParsed[i].grade = $(this).children('.column-grade').text().trim();
        gradesParsed[i].range = $(this).children('.column-range').text().trim();
        gradesParsed[i].percentage = $(this).children('.column-percentage').text().trim();
        gradesParsed[i].feedback = $(this).children('.column-feedback').text().trim();
      });
      // TODO: if parse is completely empty, don't add it
      Course.findOne({ where: { username: new RegExp(username, 'i'), courseId } }, (error, foundCourse) => {
        const gradesOld = foundCourse.gradesParsed;
        let lastChanged = foundCourse.lastChanged;
        if (gradesParsed !== gradesOld) {
          lastChanged = Date.now();
        }
        foundCourse.updateAttributes(
          { username, courseId, grades, gradesParsed, lastFetched: Date.now(), lastChanged },
          (err, instance) => {
            logger.profile(`refreshGrade ${courseId}`);
            logger.info('finishing refreshGrade', { username, courseId });
            resolve(instance);
          }
        );
      });
    });
  });
}

function refreshGradesActive(username) {
  logger.profile(`refreshGradesActive ${username}`);
  logger.info('starting refreshGradesActive', { username });
  let Course = app.models.Course;
  return new Promise((resolve, reject) => {
    Course.find({ where: { username: new RegExp(username, 'i'), active: true } }, (err, courseIds) => {
      courseIds = courseIds.map(({ courseId }) => courseId);
      let courseIdsLength = courseIds.length;
      if (courseIdsLength === 0) {
        logger.profile(`refreshGradesActive ${username}`);
        logger.info('rejected refreshGradesActive');
        return reject('no active courses');
      }
      let courseIdsPromised = courseIds.map(id => {
        return refreshGrade(username, id);
      });
      Promise.all(courseIdsPromised)
        .then(courses => {
          logger.profile(`refreshGradesActive ${username}`);
          logger.info('finishing refreshGradesActive');
          return resolve(courses);
        })
        .catch(error => reject(error));
    });
  });
}
