const cheerio = require('cheerio');
const request = require('request').defaults({ jar: true });
const loginURL = 'https://culearn.carleton.ca/moodle/login/index.php';
const headers = { 'Content-Type': 'application/x-www-form-urlencoded' };
const coursesURL = 'https://culearn.carleton.ca/moodle/my';
const app = require('../../server/server.js');
const winston = require('winston');
const logger = new winston.Logger({
  transports: [new winston.transports.Console({ timestamp: true, colorize: true })]
});
module.exports = Student => {
  Student.login = username => {
    return Student.doLogin(username);
  };
  Student.remoteMethod('login', {
    accepts: { arg: 'username', type: 'string' },
    returns: { arg: 'response', type: 'string' },
    http: { verb: 'POST' }
  });

  Student.enforceLogin = username => {
    return enforceLogin(username);
  };
  Student.remoteMethod('enforceLogin', {
    accepts: { arg: 'username', type: 'string' },
    returns: { arg: 'currentUser', type: 'string' },
    http: { verb: 'POST' }
  });

  Student.getCurrentUser = () => {
    return getCurrentUser();
  };
  Student.remoteMethod('getCurrentUser', { returns: { arg: 'username', type: 'string' }, http: { verb: 'GET' } });

  Student.doLogin = username => {
    logger.profile(`doLogin ${username}`);
    logger.info('starting doLogin', { username });
    let Student = app.models.Student;
    return new Promise((resolve, reject) => {
      Student.findOne({ where: { username: new RegExp(username, 'i') } }, (err, { password }) => {
        request.post({ url: loginURL, form: { username, password, Submit: 'login' }, headers }, (error, response) => {
          logger.profile(`doLogin ${username}`);
          logger.info('finishing doLogin', { username });
          resolve(response);
        });
      });
    });
  };
};

function enforceLogin(username) {
  logger.profile(`enforceLogin ${username}`);
  logger.info('starting enforceLogin', { username });
  let Student = app.models.Student;
  return new Promise((resolve, reject) => {
    getCurrentUser().then(currentUser => {
      if (currentUser.toLowerCase().substr(0, 3) !== username.toLowerCase().substr(0, 3)) {
        // TODO: improve username comparison
        Student.doLogin(username).then(() => getCurrentUser()).then(currentUser => {
          logger.profile(`enforceLogin ${username}`);
          logger.info('finishing enforceLogin (slow)', { username });
          resolve(currentUser);
        });
      } else {
        logger.profile(`enforceLogin ${username}`);
        logger.info('finishing enforceLogin (quick)', { username });
        resolve(currentUser);
      }
    });
  });
}

function getCurrentUser() {
  logger.profile(`getCurrentUser`);
  logger.info('starting getCurrentUser');
  return new Promise((resolve, reject) => {
    request.get(coursesURL, (error, response, body) => {
      let currentUser = cheerio.load(body)("[title='View profile']").text();
      // lazy workaround to fix problem where currentUser was "Jeremy KennedyJeremy Kennedy"
      currentUser = currentUser.slice(currentUser.length / 2);
      logger.profile(`getCurrentUser`);
      logger.info('finishing getCurrentUser', { currentUser });
      return resolve(currentUser);
    });
  });
}
